# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Git support
GIT_COMPLETION=~/.config/git/git-completion.bash
GIT_PROMPT=~/.config/git/git-prompt.sh

# https://github.com/git/git/blob/master/contrib/completion/git-completion.bash
if [[ ! -f "$GIT_COMPLETION" ]]; then
	curl -sfLo "$GIT_COMPLETION" --create-dirs \
		https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
fi

# https://github.com/lyze/posh-git-sh/blob/master/git-prompt.sh
if [[ ! -f "$GIT_PROMPT" ]]; then
	curl -sfLo "$GIT_PROMPT" --create-dirs \
		https://raw.githubusercontent.com/lyze/posh-git-sh/master/git-prompt.sh
fi

source $GIT_COMPLETION
source $GIT_PROMPT
PS_USER='\[\033[01;32m\]\u'
PS_HOST='\h'
PS_USERHOST=$PS_USER'@'$PS_HOST
PS_CURDIR='\[\033[01;34m\]\w'
PS_SIGN='\\\$ \[\033[00m\]'
PROMPT_COMMAND='__posh_git_ps1 "'$PS_USERHOST''$PS_CURDIR'" "'$PS_SIGN'";'$PROMPT_COMMAND

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi

# add intel compiler support
INTEL_COMPILER=~/intel/sw_dev_tools/compilers_and_libraries/linux/bin/compilervars.sh
if [[ -f "$INTEL_COMPILER" ]]; then
	source $INTEL_COMPILER intel64 -platform linux
fi

export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias ynab='"/home/wojtek/.wine/dosdevices/c:/Program Files (x86)/YNAB 4/YNAB 4.exe" &'

# aliases for git
alias gs='git status'
alias gc='git checkout'
alias gd='git diff'
alias gp='git pull'
alias gl='git log --oneline --graph'
